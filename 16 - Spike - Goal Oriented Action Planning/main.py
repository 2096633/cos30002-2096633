from copy import deepcopy
from random import choice

goals = {
	'Hunger': 5,
	'Fatigue': 5,
	'HouseProgress': 5,
}

actions = {
	'start': {'Fatigue': 0, 'Hunger': 0, 'HouseProgress': 0},
	'sleep': {'Fatigue': -5, 'Hunger': 1},
	'eat meat': {'Hunger': -4, 'Fatigue': 2},
	'nap': {'Fatigue': -2},
	'eat berries': {'Hunger': -2},
	'build house': {'HouseProgress': -1, 'Fatigue': 2, 'Hunger': 2}
}

#================================================================================

class WorldState(object):
	def __init__(self, goals, actions):
		self.goals = goals.copy()
		self.actions = actions
		self.action_keys = list(actions)
		self.action_index = 0

	def discontentment(self):
		discontent = 0

		for goal, value in self.goals.items():
			discontent += value * value

		return discontent

	def next_action(self):
		result = None
		if self.action_index <= len(self.actions) - 1:
			result = self.action_keys[self.action_index]
			self.action_index += 1
		return result

	def apply_action(self, action):
		for goal, change in self.actions[action].items():
			self.goals[goal] = max(self.goals[goal] + change, 0)

	def reset_index(self):
		self.action_index = 0

	def get_index_key(self, index):
		return self.action_keys[index]

	def get_goals(self):
		return self.goals

	def current_action(self):
		return self.action_keys[self.action_index]

	def get_future_discontent(self, action, p_goals, index, max_depth):
		index += 1
		if index < max_depth and action != 'start':
			temp_goals = p_goals.copy()
			#apply action
			for goal, change in self.actions[action].items():
				temp_goals[goal] = max(temp_goals[goal] + change, 0)
			#get discontent for action
			discontent = 0
			for goal, value in temp_goals.items():
			 	discontent += value * value
			#print the action and future discontent value
			tab = '   ' * (index)
			print(tab, '-+', action, '(', discontent, ')')
			for action in self.action_keys:
				self.get_future_discontent(action, temp_goals, index, max_depth)

#===============================================================================

def build_house_goap(max_depth):
	best_action = None
	best_value = 1000000
	best_plan = []

	states = [ [WorldState(goals, actions), 'start' ] ]

	while states:
		current_value = states[-1][0].discontentment()

		if len(states) >= max_depth:
			if current_value < best_value:
				best_action = states[1][1]
				best_value = current_value
				best_plan = [state[1] for state in states if state[1] ]
			states.pop()
			continue

		next_action = states[-1][0].next_action()
		if next_action:
			new_state = deepcopy(states[-1][0])
			states.append([new_state, None])
			states[-1][1] = next_action
			new_state.apply_action(next_action)
			new_state.reset_index()
		else: 
			states.pop()

	state = WorldState(goals, actions)
	i = 0
	print('PROPOSED BEST PLAN', best_plan)
	while i < max_depth-1:
		state.apply_action(state.get_index_key(i))
		print(state.get_goals())
		print('-+ CURRENT ACTION', best_plan[i], '(', state.discontentment(),')')
		print('------------------------------------------')
		j = 0
		print('-+', best_plan[i], '(', state.discontentment(), ')')
		while j < len(actions):
			if state.get_index_key(j) != 'start':
				state.get_future_discontent(state.get_index_key(j), state.get_goals(), i, max_depth)
			j += 1
		i += 1
		print('-+ BEST ACTION', best_plan[i]) #best action
		print('==========================================')


	return best_plan

#=================================================================================

def apply_action(action):
	for goal, change in actions[action].items():
		goals[goal] = max(goals[goal] + change, 0)

def print_actions():
    print('ACTIONS:')
    for name, effects in actions.items():
        print(" * [%s]: %s" % (name, str(effects)))

def get_total_discontentment():
	discontent = 0
	for goal, value in goals.items():
		discontent += value * value
	return discontent

def print_plan(plan):
	for name in plan:
		print('GOALS:', goals, 'DISCONTENTMENT:', get_total_discontentment())
		print('BEST ACTION:', name)
		apply_action(name)
		print('NEW GOALS:', goals, 'DISCONTENTMENT:', get_total_discontentment())
		print(hr)


if __name__ == '__main__':
	hr = '-'*40
	print_actions()
	print('>> Start <<')
	print(hr)
	plan = build_house_goap(3)
	print('>> Plan ready <<')
	print('>> PLAN TO EXECUTE:', plan,'<<')
	print(hr)
	
	print_plan(plan)

	print('>> Done! <<')
