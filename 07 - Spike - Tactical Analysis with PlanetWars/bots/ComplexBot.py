class ComplexBot(object):
	def update(self, gameinfo):
		# only send one fleet at a time
		# send a fleet to an enemy planet that has just sent a fleet
		if gameinfo.my_planets and gameinfo.enemy_planets and gameinfo.enemy_fleets:
			src = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)
			#dest = 
			for fleet in gameinfo.enemy_fleets:
				for p in gameinfo.my_planets:
					if src.distance_to(gameinfo.enemy_fleets[fleet].src) != 0:
						dest = gameinfo.enemy_fleets[fleet].src
						gameinfo.planet_order(src, dest, int(src.num_ships * 0.75))