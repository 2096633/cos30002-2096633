from random import choice

class LargestoGrowthoBoto(object):
	def update(self, gameinfo):
		# only send one fleet at a time
		if gameinfo.my_planets and gameinfo.not_my_planets:
			dest = max(gameinfo.not_my_planets.values(), key=lambda p: p.growth_rate) #finds the strongest enemy planet
			src = choice(list(gameinfo.my_planets.values())) #gets its strongest planet in its possession
			gameinfo.planet_order(src, dest, int(src.num_ships * 0.75)) #sends ships from strongest planet to strongest enemy planet