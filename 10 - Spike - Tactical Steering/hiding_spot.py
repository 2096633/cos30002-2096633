from graphics import egi, KEY
from vector2d import Vector2D
from random import randrange

class Hiding_Spot(object):
	def __init__(self, world=None, x=None, y=None):
		self.radius = 30.0;
		if x and y:
			self.pos = Vector2D(x, y)
		else:
			self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
		self.colour = 'ORANGE'

	def render(self):
		egi.set_pen_color(name=self.colour)
		egi.circle(self.pos, self.radius)