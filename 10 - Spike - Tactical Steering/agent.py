'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from hiding_spot import Hiding_Spot

class Agent(object):

    # NOTE: Class Object (not *instance*) variables! --- reversed for security :)
    DECELERATION_SPEEDS = {
        'slow': 0.9,
        'normal': 1,
        'fast': 1.1,
    }

    def __init__(self, world=None, scale=30.0, mass=1.0, mode='wander'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass
        self.targets = []
        self.target = None

        # data for drawing this agent
        self.color = 'GREEN'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.6),
            Point2D( 1.0, -0.6),
            Point2D(-1.0, -0.6),
        ]

        # limits?
        self.max_speed = 20.0 * scale
        self.max_force = 500.0

        ### wander details
        self.wander_target = Vector2D(1,0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale

        # debug draw info?
        self.show_info = False

    def calculate(self, delta):
        # calculate the current steering force
        mode = self.mode
        if mode == 'flee':
            force = self.flee(self.world.hunter.pos)
        elif mode == 'arrive':
            force = self.arrive(self.target_pos, 'fast')
        elif mode == 'wander':
            force = self.wander(delta, self.world.hunter.pos)
        elif mode == 'seek' and self.world.target:
            force = self.seek(self.world.target)
        else:
            force = Vector2D()
        self.accel = force * self.mass
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander
        ## limit force? <-- for wander
        force.truncate(self.max_force)
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        for t in self.targets:
            egi.white_pen()
            egi.cross(t.pos, 10)

        # draw wander info?
        if self.mode == 'wander':
            # calculate the center of the wander circle in front of the agent
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            # draw the wander circle
            egi.green_pen()
            egi.circle(wld_pos, self.wander_radius)
            # draw the wander target (little circle on the big circle)
            egi.red_pen()
            wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)
            pass

        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        panic_range = 250
        if self.pos.distance(self.world.hunter.pos) < panic_range:
            self.mode = 'flee'

        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def flee(self, hunter_pos):
        ''' select hiding spot and move there '''
        range = 400
        #egi.set_pen_color(name="YELLOW")
        #egi.circle(self.pos, range)
        targets = []
        if self.target:
            target = self.target
        else:
            target = None
            # check which hiding spot is closest
            for spot in self.world.hiding_spots:
                if self.pos.distance(spot.pos) < range:
                    self.targets.append(spot)
                    targets.append(spot)

            if targets:
                target = targets[0]
                for spot in targets:        
                    if hunter_pos.distance(spot.pos) > hunter_pos.distance(target.pos):
                        target = spot

        if target is not None:
            self.target = target
            # get the heading of the closest hiding spot and the hunter
            heading = target.pos - hunter_pos
            # normalise so magnitude becomes 1
            spot_pos = heading.normalise()
            # add the circumfrance of the hiding spot so the agent 'hides behind' it
            extended = spot_pos * (target.radius * 2)
            # apply the extension
            target_pos = target.pos + extended
            #draw a cross for visual representation of where the agent is hiding
            egi.set_pen_color(name="PURPLE")
            egi.cross(target_pos, 10)
        else:
            return Vector2D()
        # set arrive target to be the extended position
        desired_vel = self.arrive(target_pos, 'fast')

        self.target.colour = 'LIGHT_BLUE'

        # if the hunter is far away and this is at target -> wander again
        if self.pos.distance(hunter_pos) > 300 and self.pos.distance(target_pos) < self.scale.x:
            self.mode = 'wander'
            target.colour = 'ORANGE'
            self.targets = []
            self.target = None
            self.world.target = None

        return desired_vel

    def arrive(self, target_pos, speed):
        ''' this behaviour is similar to seek() but it attempts to arrive at
            the target position with a zero velocity'''
        decel_rate = self.DECELERATION_SPEEDS[speed]
        to_target = target_pos - self.pos
        dist = to_target.length()
        if dist > 0:
            # calculate the speed required to reach the target given the
            # desired deceleration rate
            speed = dist / decel_rate
            # make sure the velocity does not exceed the max
            speed = min(speed, self.max_speed)
            # from here proceed just like Seek except we don't need to
            # normalize the to_target vector because we have already gone to the
            # trouble of calculating its length for dist.
            desired_vel = to_target * (speed / dist)
            return (desired_vel - self.vel)
        return Vector2D(0, 0)

    def wander(self, delta, hunter_pos):
        ''' Random wandering using a projected jitter circle. '''
        if self.world.target:
            self.mode = 'seek'

        panic_range = 250
        if self.pos.distance(hunter_pos) < panic_range:
            self.mode = 'flee'

        wt = self.wander_target
        # this behaviour is dependent on the update rate, so this line must
        # be included when using time independent framerate.
        jitter_tts = self.wander_jitter * delta
        # first, add a small random vector to the target's position
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        # re-project this new vector back on to a unit circle
        wt.normalise()
        # increase the length of the vector to the same as the radius
        # of the wander circle
        wt *= self.wander_radius
        # move the target into a position WanderDist in front of the agent
        target = wt + Vector2D(self.wander_dist, 0)
        # project the taget into the world space
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        # and steer towards it
        return self.seek(wld_target)
