# variables
distance = 0
fatigue = 0
items = 0

states = ['wander','sit','collect']
current_state = 'wander'

running = True
max_limit = 100
game_time = 0

while running:
	game_time += 1
	
	#wander
	if current_state is 'wander':
		print("wandering...")
		distance += 1
		fatigue += 1
		if distance > 10:
			current_state = 'collect'
		if fatigue > 13:
			current_state = 'sit'
			
	#sit		
	elif current_state is 'sit':
		print("sitting down")
		fatigue -= 1
		if fatigue < 5:
			if distance > 10:
				current_state = 'collect'
			else:
				current_state = 'wander'
	
	#collect
	elif current_state is 'collect':
		print("item collected!")
		items += 1
		fatigue += 1
		distance = 0
		if fatigue > 13:
			current_state = 'sit'
		else:
			current_state = 'wander'
	
	#broken
	else:
		print("FSM did done broke somehow")
		die() #to break things?
		
	if game_time > max_limit:
		running = False;
	
print('%s items collected' % (items)) 
print("-- The End --")
input("Press enter to close...")
