from random import randrange

board = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']

winner = None
gameOver = False
playerChar = { 
	'X': 'Human',
	'O': "AI",
}
currentPlayer = ''
move = None

WIN_SET = (
    (0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
    (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)
)

def checkMove():
	global move
	try:
		move = int(move)
		if board[move] == ' ':
			return True
		else:
			print("Invalid move, please try again")
			return False
	except:
		print('%s is not a valid position. Please enter a number between 0 and 8' % move)
		return False

def checkWinConditions():
	for row in WIN_SET:
		if board[row[0]] == board[row[1]] == board[row[2]] != ' ':
			return board[row[0]]

	if ' ' not in board:
		return 'tie'

	return None

def humanHelp():
	help = '''
To make a move enter a number between 0 - 8, based on the board below and press enter.

	0 | 1 | 2
	---------
	3 | 4 | 5
	---------
	6 | 7 | 8
	'''
	print(help)

def humanInput():
	return input('[0-8] :> ')

def aiMove():
	return randrange(9)

def blockAiMove():
	for row in WIN_SET:
		if board[row[0]] == board[row[1]] == 'X' and board[row[2]] == ' ':
			return row[2]
		if board[row[1]] == board[row[2]] == 'X' and board[row[0]] == ' ':
			return row[0]
		if board[row[0]] == board[row[2]] == 'X' and board[row[1]] == ' ':
			return row[1]

	return randrange(9)

def update():
	global currentPlayer, move, winner

	if currentPlayer == 'X':
		move = humanInput()
	else:
		move = blockAiMove()
	
	if checkMove():
		board[move] = currentPlayer
		winner = checkWinConditions()
		if currentPlayer == 'X':
			currentPlayer = 'O'
		else:
			currentPlayer = 'X'
	else:
		print("Try again")


def render():
	print("\n----------------------\n")
	print("	%s|%s|%s" % tuple(board[:3]))
	print("	-----")
	print("	%s|%s|%s" % tuple(board[3:6]))
	print("	-----")
	print("	%s|%s|%s" % tuple(board[6:9]))
				
		
if __name__ == '__main__':
	humanHelp()

	render()
	currentPlayer = 'X'

	while winner is None:
		update()
		render()

	if winner is 'tie':
		print("Game ends in a tie!")
	elif winner in playerChar:
		print('%s is the winner!' % playerChar[winner])
	