from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from projectile import Projectile

SHOOTER_MODES = {
    KEY._1: 'rifle',
    KEY._2: 'rocket',
    KEY._3: 'hand_gun',
    KEY._4: 'grenade',
}

class Shooter(object):
	def __init__(self, world=None, scale=30.0, mode='rifle'):
		self.world = world
		self.mode = mode

		dir = radians(random() * 360)
		self.pos = Vector2D(250, 250)
		self.heading = Vector2D(sin(dir), cos(dir))
		self.side = self.heading.perp()
		self.scale = Vector2D(scale, scale)

		self.colour = 'LIGHT_BLUE'
		self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.6),
            Point2D( 1.0, -0.6),
            Point2D(-1.0, -0.6),
        ]

		self.show_info = False

	def update(self):
		if self.world.projectile is None:
			mode = self.mode
			if mode == 'rifle':
				self.fire_projectile('fast', True)
				pass
			elif mode == 'rocket':
				self.fire_projectile('slow', True)
				pass
			elif mode == 'hand_gun':
				self.fire_projectile('fast', False)
				pass
			elif mode == 'grenade':
				self.fire_projectile('slow', False)
				pass
			else:
				pass

	def render(self):
		egi.set_pen_color(name=self.colour)
		pts = self.world.transform_points(self.vehicle_shape, self.pos, self.heading, self.side, self.scale)
		egi.closed_shape(pts)

	def fire_projectile(self, speed, accurate):
		self.world.projectile = Projectile(self.world, self.pos, speed, accurate)