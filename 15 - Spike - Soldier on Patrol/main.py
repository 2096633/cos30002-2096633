'''Autonomous Agent Movement: Paths and Wandering

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

This code is essentially the same as the base for the previous steering lab
but with additional code to support this lab.

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from agent import Agent, AGENT_MODES  # Agent with seek, arrive, flee and pursuit
from shooter import Shooter, SHOOTER_MODES


def on_mouse_press(x, y, button, modifiers):
    '''
    test to see if the shooter moves to the seek target and then returns to patroling 
    if there is no target at the spotted position (code in shooter.py needs to be modified)
    for this to work correctly and as such it is commented out
    '''
    if button == 1:  # left
        #world.shooter.current_state = 'seek'
        #world.shooter.seek_target = Vector2D(x, y)
        pass


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol in SHOOTER_MODES:
        world.shooter.mode = SHOOTER_MODES[symbol]
    elif symbol == KEY.A:
        world.agents.append(Agent(world))
    # Toggle debug force line info on the agent
    elif symbol == KEY.I:
        if world.agent:
            world.agent.show_info = not world.agent.show_info

def on_resize(cx, cy):
    world.cx = cx
    world.cy = cy


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=1920, height=1080, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = clock.ClockDisplay()
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_press)
    win.push_handlers(on_resize)

    # create a world for agents
    world = World(1920, 1080)
    # add one agent
    world.agents.append(Agent(world))
    # add shooter to world
    world.shooter = Shooter(world)
    # unpause the world ready for movement
    world.paused = False

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()

