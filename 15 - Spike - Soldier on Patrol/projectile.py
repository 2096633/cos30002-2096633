from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from random import random, randrange
from math import sin, cos, radians

class Projectile(object):
	PROJECTILE_SPEED = {
		'fast': 200,
		'slow': 50,
	}


	def __init__(self, world=None, pos=None, speed='fast', accurate=True, target=None, scale=15.0):
		self.pos = pos * 1
		self.world = world
		self.scale = Vector2D(scale, scale)
		self.max_speed = self.PROJECTILE_SPEED[speed] * scale
		self.max_force = 250.0
		self.vel = (target.pos - self.pos).normalise() * self.max_speed
		self.heading = self.vel.get_normalised()
		self.side = self.heading.perp()
		self.force = Vector2D()
		self.accel = Vector2D()
		self.accurate = accurate

		self.target = target
		self.kill = False

		self.bRadius = scale

		self.colour = 'WHITE'
		self.shape = [
			Point2D(-1.0, 0),
			Point2D(1.0, 0),
		]
	
	def update(self, delta):
		self.kill = self.detect_collision()
		
		force = self.seek(self.target.pos)
		self.force = force
		force.truncate(self.max_force)
		self.accel = force
		self.vel += self.accel * delta
		self.vel.truncate(self.max_speed)
		self.pos += self.vel * delta

		if self.vel.length_sq() > 0.0000001:
			self.heading = self.vel.get_normalised()
			self.side = self.heading.perp()

	def render(self):
		egi.set_pen_color(name=self.colour)
		pts = self.world.transform_points(self.shape, self.pos, self.heading, self.side, self.scale)
		egi.closed_shape(pts)

	def seek(self, target):
		desired_vel = (target - self.pos).normalise() * self.max_speed
		return (desired_vel - self.vel)

	def detect_collision(self):
		kill = False
		if self.pos.x < 0 or self.pos.x > self.world.cx or self.pos.y < 0 or self.pos.y > self.world.cy:
			kill = True
		for agent in self.world.agents:
			if self.pos.distance(agent.pos) < self.bRadius + agent.bRadius:
				agent.hit = True
				kill = True

		if self.target:
			if self.pos.distance(self.target.pos) < self.bRadius:
				kill = True
		return kill

	def get_projectile_target(self, target):
		to_target = target.pos - self.world.shooter.pos
		
		look_ahead_time = to_target.length() / (self.max_speed + target.speed())

		look_ahead_pos = target.pos + target.vel * look_ahead_time

		if not self.accurate:
			x = randrange(-10, 10)
			y = randrange(-10, 10)
			look_ahead_pos.x += x
			look_ahead_pos.y += y

		self.world.projectile_target = look_ahead_pos
		self.seek(look_ahead_pos)