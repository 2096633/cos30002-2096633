from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from projectile import Projectile
from path import Path

SHOOTER_MODES = {
    KEY._1: 'rifle',
    KEY._2: 'rocket',
    KEY._3: 'hand_gun',
    KEY._4: 'grenade',
}

class Shooter(object):
	SHOOTER_STATES = [
		'patrol',
		'seek',
		'attack',
	]


	def __init__(self, world=None, scale=30.0, mode='rifle'):
		self.world = world
		self.mode = mode

		self.current_state = 'patrol'

		dir = radians(random() * 360)
		self.pos = Vector2D(250, 250)
		self.heading = Vector2D(sin(dir), cos(dir))
		self.side = self.heading.perp()
		self.scale = Vector2D(scale, scale)

		self.vel = Vector2D()
		self.accel = Vector2D()

		self.seek_speed = 10.0 * scale
		self.patrol_speed = 5.0 * scale
		self.max_speed = self.patrol_speed
		self.max_force = 500.0

		self.seek_target = None
		self.seek_radius = 500.0
		self.attack_radius = 300.0
		self.bRadius = scale

		self.attack_rate = 10.0
		self.attack_cooldown = 0.0

		self.colour = 'LIGHT_BLUE'
		self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.6),
            Point2D( 1.0, -0.6),
            Point2D(-1.0, -0.6),
        ]

		### path to follow?
		self.path = Path(looped=True)
		self.create_path()
		self.waypoint_threshold = 50

		self.show_info = False

	def update(self, delta):
		force = Vector2D()
		self.attack_cooldown -= 0.5
		#print(self.current_state)
		if self.current_state is 'patrol':
			self.max_speed = self.patrol_speed
			force = self.follow_path()

			for agent in self.world.agents:
				if self.pos.distance(agent.pos) <= self.attack_radius:
					self.current_state = 'attack'
				elif self.pos.distance(agent.pos) <= self.seek_radius:
					self.current_state = 'seek'
		elif self.current_state is 'seek':
			self.max_speed = self.seek_speed
			for agent in self.world.agents:
				if self.pos.distance(agent.pos) <= self.attack_radius:
					self.current_state = 'attack'
					return
				elif self.pos.distance(agent.pos) <= self.seek_radius:
					self.seek_target = agent.pos.copy()
					force = self.seek(self.seek_target)
				elif self.seek_target:
					if self.pos.distance(self.seek_target) < self.bRadius:
						self.current_state = 'patrol'
		elif self.current_state is 'attack':
			target = None
			for agent in self.world.agents:
				if self.pos.distance(agent.pos) <= self.attack_radius:
					target = agent
			if self.attack_cooldown <= 0 and target is not None:
				mode = self.mode
				if mode == 'rifle':
					self.fire_projectile('fast', True, target)
					pass
				elif mode == 'rocket':
					self.fire_projectile('slow', True, target)
					pass
				elif mode == 'hand_gun':
					self.fire_projectile('fast', False, target)
					pass
				elif mode == 'grenade':
					self.fire_projectile('slow', False, target)
					pass
				else:
					print("something went wrong and the shot type is all done broked")
				self.attack_cooldown = self.attack_rate

			if len(self.world.agents) > 0:
				for agent in self.world.agents:
					if self.pos.distance(agent.pos) >= self.seek_radius:
						self.current_state = 'patrol'
					elif self.pos.distance(agent.pos) >= self.attack_radius:
						self.current_state = 'seek'
			else:
				self.current_state = 'patrol'
		else:
			print("something went wrong and the states all done broked")

		#move agent
		force.truncate(self.max_force)
		self.vel += force * delta
		self.vel.truncate(self.max_speed)
		self.pos += self.vel * delta

		if self.vel.length_sq() > 0.00000001:
			self.heading = self.vel.get_normalised()
			self.side = self.heading.perp()

		self.world.wrap_around(self.pos)

	def render(self):
		self.path.render()

		egi.set_pen_color(name=self.colour)
		pts = self.world.transform_points(self.vehicle_shape, self.pos, self.heading, self.side, self.scale)
		egi.closed_shape(pts)

		egi.set_pen_color(name='YELLOW')
		egi.circle(self.pos, self.seek_radius)

		egi.set_pen_color(name='BROWN')
		egi.circle(self.pos, self.attack_radius)

		if self.seek_target and self.current_state is 'seek':
			egi.set_pen_color(name='RED')
			egi.cross(self.seek_target, 5)

	def create_path(self):
		self.path.add_way_pt(Vector2D(250,250))
		self.path.add_way_pt(Vector2D(500,250))
		self.path.add_way_pt(Vector2D(500,500))
		self.path.add_way_pt(Vector2D(1500,1000))
		self.path.add_way_pt(Vector2D(250,1000))
		self.path.add_way_pt(Vector2D(250,250))

	def seek(self, target):
		desired_vel = (target - self.pos).normalise() * self.max_speed
		return (desired_vel - self.vel)	

	def follow_path(self):
		'''
		if heading to final point (is_finished)
			return a slow down force vector (arrive)
		else
			if within threshold distance of current way point, inc to next in path
			return a force vector to head to current point at full speed (seek)
		'''
		if self.path.is_finished() is True:
			return self.arrive(self.path.current_pt(), 'normal')
		else:
			to_target = self.path.current_pt() - self.pos
			dist = to_target.length()
			if dist < self.waypoint_threshold:
				self.path.inc_current_pt()
			return self.seek(self.path.current_pt())

	def fire_projectile(self, speed, accurate, target):
		self.world.projectiles.append(Projectile(self.world, self.pos, speed, accurate, target))