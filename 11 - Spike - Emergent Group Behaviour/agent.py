'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path

AGENT_MODES = {
    KEY._1: 'seek',
    KEY._2: 'arrive_slow',
    KEY._3: 'arrive_normal',
    KEY._4: 'arrive_fast',
    KEY._5: 'flee',
    KEY._6: 'pursuit',
    KEY._7: 'follow_path',
    KEY._8: 'wander',
}

class Agent(object):

    # NOTE: Class Object (not *instance*) variables! --- reversed for security :)
    DECELERATION_SPEEDS = {
        'slow': 0.9,
        'normal': 1,
        'fast': 1.1,
    }

    MAX_STEERING_FORCE = 400.0

    def __init__(self, world=None, scale=30.0, mass=1.0, mode='wander'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass
        self.tagged = False

        # data for drawing this agent
        self.color = 'ORANGE'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]
        ### path to follow?
        self.path = Path()
        self.randomise_path()
        self.waypoint_threshold = 50

        ### wander details
        self.wander_target = Vector2D(1,0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = 5.0 * scale

        # limits?
        self.max_speed = 10.0 * scale
        self.max_force = 500.0

        self.radius = 100.0

        # debug draw info?
        self.show_info = True

    def calculate(self, delta):
        # calculate the current steering force
        self.tag_neighbours(self.world.agents, self.radius)
        steering_force = Vector2D()
        # call the current steering behaviour
        steering_force += self.wander(delta) * self.world.wander_amount
        #steering_force += self.seek(Vector2D(0,0))
        steering_force += self.cohesion(self.world.agents) * self.world.cohesion_amount
        steering_force += self.seperation(self.world.agents) * self.world.seperation_amount
        steering_force += self.alignment(self.world.agents) * self.world.alignment_amount

        #self.force = steering_force
        return steering_force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander
        ## limit force? <-- for wander
        force.truncate(self.MAX_STEERING_FORCE)
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            #self.heading = self.alignment(self.world.agents).get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        # draw the path if it exists and the mode is follow
        if self.mode == 'follow_path':
            self.path.render()
            pass

        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        # draw wander info?
        if self.mode == 'wander':
            # calculate the center of the wander circle in front of the agent
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            # draw the wander circle
            egi.green_pen()
            egi.circle(wld_pos, self.wander_radius)
            # draw the wander target (little circle on the big circle)
            egi.red_pen()
            wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)
            pass

        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

            egi.green_pen()
            egi.circle(self.pos, self.radius + self.bRadius)

    def speed(self):
        return self.vel.length()

    def randomise_path(self):
        cx = self.world.cx
        cy = self.world.cy
        margin = min(cx, cy) * (1/6)
        self.path.create_random_path(4, 0 + margin, 0 + margin, cx - margin, cy - margin)

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target
        # this behaviour is dependent on the update rate, so this line must
        # be included when using time independent framerate.
        jitter_tts = self.wander_jitter * delta
        # first, add a small random vector to the target's position
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        # re-project this new vector back on to a unit circle
        wt.normalise()
        # increase the length of the vector to the same as the radius
        # of the wander circle
        wt *= self.wander_radius
        # move the target into a position WanderDist in front of the agent
        target = wt + Vector2D(self.wander_dist, 0)
        # project the taget into the world space
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        # and steer towards it
        return self.seek(wld_target)

    def tag_neighbours(self, bots, radius):
        # lets find 'em
        for bot in bots:
            if bot is not self:
                # untag all first
                bot.tagged = False
                # take into account the bounding radius
                gap = radius + bot.bRadius
                if self.pos.distance(bot.pos) < gap:
                    bot.tagged = True

                if bot == self.world.agents[0]:
                    for bot in bots:
                        bot.color = 'WHITE'

    def seperation(self, group):
        steering_force = Vector2D()
        for bot in group:
            # don't include self, only include neighbours (already tagged)
            if bot != self and bot.tagged:
                to_bot = self.pos - bot.pos
                # scale based on inverse distance to neighbour
                steering_force += to_bot.get_normalised() / to_bot.length()
        return steering_force * self.max_speed

    def cohesion(self, group):
        center_mass = Vector2D()
        steering_force = Vector2D()
        avg_count = 0

        for bot in group:
            if bot != self and bot.tagged:
                center_mass += bot.pos
                avg_count += 1

        if avg_count > 0:
            center_mass /= float(avg_count)
            steering_force = self.seek(center_mass)

        return steering_force

    def alignment(self, group):
        avg_heading = Vector2D()
        avg_count = 0

        for bot in group:
            if bot != self and bot.tagged:
                avg_heading += bot.heading
                avg_count += 1

        if avg_count > 0:
            avg_heading /= float(avg_count)
            avg_heading -= self.heading

        return avg_heading